### Lightning Network Quick Start

> Accepting Lightning Network payments is advanced feature for power users willing to manage their Lightning Network daemon.


#### Prerequisites
- You have CypherpunkPay installed and running
- You have your LND running with channel(s) open and sufficient incoming capacity

#### Understanding

- You are expected to setup and manage your LND outside of CypherpunkPay (although obviously this could be on the same server)
- Your LND wallet and CypherpunkPay wallet are separate wallets
- CypherpunkPay uses a small subset of LND’s HTTP REST API to create LN invoices and to ask for their status

### Config

Add this to your **/etc/cypherpunkpay.conf**:

    [lightning-network]
    
    btc_lightning_enabled = true
    
    # The protocol://host:port for the LND node.
    # Note:
    #   * we change LND's default 8080 port because it is already taken by CypherpunkPay in production
    #   * LND enforces TLS even on the localhost by default, hence https
    btc_lightning_lnd_url = https://127.0.0.1:8081/
    
    # CypherpunkPay will authenticate to LND with this token in an HTTP header:
    # Grpc-Metadata-macaroon: <btc_lightning_lnd_invoice_macaroon>
    # You can get the value by dumping relevant LND *.macaroon file into hex:
    # import codecs; codecs.encode(open('/home/lnd/.lnd/data/chain/bitcoin/testnet/invoice.macaroon', 'rb').read(), 'hex')
    btc_lightning_lnd_invoice_macaroon = 0201036c6e640258030a1048e0f05f02ff3f5ff7a4046ba186016e1201301a160a0761646472657373120472656164120577726974651a170a08696e766f69636573120472656164120577726974651a0f0a076f6e636861696e120472656164000006208778b5873594d4033de9b6b945d56e5b09cb00bb6001208d4366e8b348bd6a5f
    


### Adjust URL to LND

Adjust URL to your LND (btc_lightning_lnd_url) in above config.

Note:

- By default LND expects TLS even on 127.0.0.1 (hence weird https://127.0.0.1:8081 in the example)

- LND’s default port 8080 will clash with CypherpunkPay, so you need to change it (hence https://127.0.0.1:8081)
 		Don’t forget to also change it in LND’s config and restart LND


### Adjust macaroon auth
Adjust the **btc_lightning_lnd_invoice_macaroon** - it is LND’s authentication mechanism.

Your LND generated this at **~/.lnd/data/chain/bitcoin/testnet/invoice.macaroon**.

To get hex value you can process the binary file as suggested in the comments above.


### Restart CypherpunkPay and test

		sudo systemctl restart cypherpunkpay
After restart you should be able to pick “Bitcoin (BTC) Lightning Network” on the coin selection step.

### Debugging
Watch CypherpunkPay logs for specific error messages:

		sudo journalctl -xe --unit cypherpunkpay.service


asd
