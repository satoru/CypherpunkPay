Put CypherpunkPay behind a reverse proxy
========================================

### Come up with a domain[](#come-up-with-a-domain)

We will assume `cypherpunkpay.yourwebsite.com` is resolving to your CypherpunkPay server. **Make sure it does before proceeding**. Obviously, change the example domain to your liking.

It is also possible to have CypherpunkPay on the same domain as your existing website, in a subpath, like `yourwebsite.com/cypherpunkpay/`. It is not perfect because the two apps would share cookies, etc.

### Configure web server as reverse proxy[](#configure-web-server-as-reverse-proxy)

We recommend Caddy because it automates TLS certificates and is very easy to use.

 Caddy

#### Install Caddy[](#install-caddy)

This is copied from Caddy’s official [docs](https://caddyserver.com/docs/install#debian-ubuntu-raspbian) for your convenience.

    curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/gpg.key' | sudo apt-key add -
    curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/debian.deb.txt' | sudo tee -a /etc/apt/sources.list.d/caddy-stable.list
    sudo apt update
    sudo apt install caddy
    

You can verify Caddy is working with `sudo systemctl status caddy`.

#### Configure Caddy as reverse proxy[](#configure-caddy-as-reverse-proxy)

Open `/etc/caddy/Caddyfile`. The default config is almost perfect!

Replace `:80` with your domain `cypherpunkpay.yourwebsite.com`.

Uncomment `reverse_proxy localhost:8080` line.

Add `encode zstd gzip` line (optional but recommended).

Comment out other features (root, file\_server - we don’t need them).

#### Restart Caddy[](#restart-caddy)

    sudo systemctl restart caddy
    

That’s all!

Caddy will create TLS certificate for your domain. Just make sure your domain resolves corectly, and your server is accessible from the outside (no firewall blocking 80, 443).

 Nginx

#### Install Nginx[](#install-nginx)

    sudo apt-get install nginx
    

Excellent.

#### Configure Nginx as reverse proxy[](#configure-nginx-as-reverse-proxy)

Create the config file (adjust the domain):

    sudo nano /etc/nginx/sites-available/cypherpunkpay.conf
    

    server {
        listen 80;
        server_name cypherpunkpay.yourwebsite.com;
    
        gzip_types
            text/css
            text/plain
            text/javascript
            application/javascript
            application/json;
    
        location / {
            proxy_pass http://127.0.0.1:8080;
        
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
        }
    }
    

Enable this configuration:

    sudo ln -s /etc/nginx/sites-available/cypherpunkpay.conf /etc/nginx/sites-enabled/cypherpunkpay.conf 
    

That is for the HTTP version. We will cover HTTPS shortly.

#### Restart nginx and verify HTTP[](#restart-nginx-and-verify-http)

    sudo systemctl restart nginx
    

Go to `http://cypherpunkpay.yourwebsite.com/` - you should already have an HTTP version up and running correctly.

#### Add HTTPS support[](#add-https-support)

Remember to adjust your domain.

    sudo apt-get install certbot python3-certbot-nginx
    sudo certbot --nginx -d cypherpunkpay.yourwebsite.com
    

Answer certbot interactive questions (yes, redirect HTTP to HTTPS).

#### Restart Nginx and verify HTTPS[](#restart-nginx-and-verify-https)

    sudo systemctl restart nginx
    

### Congratulations![](#congratulations)

What we achieved here:

*   CypherpunkPay is production live at `https://cypherpunkpay.yourwebsite.com/`
    
*   CypherpunkPay is publicly accessible with a valid domain TLS certificate.
    
*   CypherpunkPay runs in **donations mode** (the default).
    
*   CypherpunkPay runs on **Bitcoin testnet** (so you can play safely).
    
*   A **testnet wallet got created** and configured for you during package installation.
    
