Bitcoin Full Node
=================

> Running a full node is **highly encouraged** but not required by CypherpunkPay.
> 
> CypherpunkPay has mature support for checking payments against random pairs of block explorers, over the Tor network. You can start light and easy and upgrade your setup when ready.

### Why full node?[](#why-full-node)

Running your own full node has significant advantages:

*   Ultimate certainty you received the real Bitcoin
*   More privacy as no 3rd party is ever queried (even if only over Tor)
*   Better UX with faster payments recognition if full node is running locally (no calls over Tor necessary)
*   In case of a network fork, control over which rules to follow

### Is pruned node supported?[](#is-pruned-node-supported)

**Yes!** Pruned nodes are fully supported. Last time we checked, you will only need **~6GB** for mainnet and **~2.3GB** for testnet.

The slight drawback is your full node (and by extension CypherpunkPay) will only be guaranteed aware of the most recent ~3.5 days of confirmed transactions (550 blocks with default settings). This shouldn’t be a problem unless you experience a 4 days of downtime. You can mitigate this risk by increasing the number of blocks with `prune=5000` (this gives ~30+ days).

### Which Bitcoin full node implementations are supported?[](#which-bitcoin-full-node-implementations-are-supported)

CypherpunkPay was tested against **Bitcoin Core v0.21.1**. CypherpunkPay works with JSON/RPC API exposed by `bitcoind`.

### What are my server options?[](#what-are-my-server-options)

You can:

*   Install Bitcoin Core on the same machine as CypherpunkPay. This is the easiest option as no authentication is necessary.
*   Install Bitcoin Core on a separate machine within your private network. Authentication must be done by the network layer, maybe with WireGuard or IPsec.
*   Install on a remote machine and expose as onion service with onion authentication. Authentication must be done by the Tor daemon. By far the most flexible setup but calls will be much slower and so payment recognition not as swift.

### Step 1: Install and sync Bitcoin Core[](#step-1-install-and-sync-bitcoin-core)

We will assume local Bitcoin Core (on the same machine as CypherpunkPay).

Use your prefered method to install [Bitcoin Core](https://bitcoincore.org/en/download/).

If not installing from packages, remember to verify the signatures.

**Optionally** add `prune=550` to `/etc/bitcoin.conf` if you want a pruned node (or `prune=5000` to have guaranteed ~30+ days of confirmed tx history).

Restart `bitcoind` and **wait** for the initial block download. This will take between hours and days (regardless of pruning).

CypherpunkPay won’t work until your node is fully synced.

### Step 2: Enable Bitcoin Core RPC server[](#step-2-enable-bitcoin-core-rpc-server)

In your `/etc/bitcoin.conf` add or uncomment default settings for the RPC interface:

    # mainnet
    
    # [rpc]
    server=1
    rpcport=8332
    rpcuser=bitcoin
    rpcpassword=secret
    rpcallowip=127.0.0.1
    rpcbind=127.0.0.1
    

Remember to restart `bitcoind` daemon.

You should check `debug.log` to make sure Bitcoin Core started as expected and does listen on `127.0.0.1:8332`.

### Step 3: Instruct CypherpunkPay to use your node[](#step-3-instruct-cypherpunkpay-to-use-your-node)

In `/etc/cypherpunkpay.conf` add or uncomment:

    [full node]
    
    btc_mainnet_node_enabled = true
    btc_mainnet_node_rpc_url = http://127.0.0.1:8332
    btc_mainnet_node_rpc_user = bitcoin
    btc_mainnet_node_rpc_password = secret
    

### 4\. Restart CypherpunkPay[](#4-restart-cypherpunkpay)

    sudo systemctl restart cypherpunkpay
    

### 5\. Review CypherpunkPay logs[](#5-review-cypherpunkpay-logs)

You should see a confirmation CypherpunkPay connected to your full node.

    sudo journalctl -xe --unit cypherpunkpay.service
    

### Congratulations![](#congratulations)

What we achieved here:

*   CypherpunkPay will use your full node to recognize payments and network confirmations.
*   No external block explorers will ever be called (as long as `btc_mainnet_node_enabled = true`).
