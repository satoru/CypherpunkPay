Quick Start
===========

### Prerequisites[](#prerequisites)

*   Ubuntu or Debian Linux with root access
*   For production deployment: any VPS with 1 CPU, 1GB RAM, SSD will do

### Add CypherpunkPay repository (deb.cypherpunkpay.org)[](#add-cypherpunkpay-repository-debcypherpunkpayorg)

 Ubuntu 20.04

    # Add cypherpunkpay package signing key
    wget -qO - https://deb.cypherpunkpay.org/cypherpunkpay-package-signer.asc | gpg --dearmor | sudo tee /usr/share/keyrings/cypherpunkpay-archive-keyring.gpg 1> /dev/null
    
    # Add cypherpunkpay repository
    echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/cypherpunkpay-archive-keyring.gpg] https://deb.cypherpunkpay.org/apt/ubuntu/ focal main' | sudo tee /etc/apt/sources.list.d/cypherpunkpay.list
    

 Ubuntu 18.04

    # Add cypherpunkpay package signing key
    wget -qO - https://deb.cypherpunkpay.org/cypherpunkpay-package-signer.asc | gpg --dearmor | sudo tee /usr/share/keyrings/cypherpunkpay-archive-keyring.gpg 1> /dev/null
    
    # Add cypherpunkpay repository
    echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/cypherpunkpay-archive-keyring.gpg] https://deb.cypherpunkpay.org/apt/ubuntu/ bionic main' | sudo tee /etc/apt/sources.list.d/cypherpunkpay.list
    

 Debian 11

    # Add cypherpunkpay package signing key
    wget -qO - https://deb.cypherpunkpay.org/cypherpunkpay-package-signer.asc | gpg --dearmor | sudo tee /usr/share/keyrings/cypherpunkpay-archive-keyring.gpg 1> /dev/null
    
    # Add cypherpunkpay repository
    echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/cypherpunkpay-archive-keyring.gpg] https://deb.cypherpunkpay.org/apt/debian/ bullseye main' | sudo tee /etc/apt/sources.list.d/cypherpunkpay.list
    

 Debian 10

    # Add cypherpunkpay package signing key
    wget -qO - https://deb.cypherpunkpay.org/cypherpunkpay-package-signer.asc | gpg --dearmor | sudo tee /usr/share/keyrings/cypherpunkpay-archive-keyring.gpg 1> /dev/null
    
    # Add cypherpunkpay repository
    echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/cypherpunkpay-archive-keyring.gpg] https://deb.cypherpunkpay.org/apt/debian/ buster main' | sudo tee /etc/apt/sources.list.d/cypherpunkpay.list
    

### Install CypherpunkPay[](#install-cypherpunkpay)

    # Update apt indexes
    sudo apt-get update
    
    # Install cypherpunkpay
    sudo apt-get install cypherpunkpay
    

### Start CypherpunkPay[](#start-cypherpunkpay)

> For this Quick Start we assume you are experimenting on your laptop or inside a VM with desktop user interface.

    # (Re)start cypherpunkpay
    sudo systemctl restart cypherpunkpay
    
    # Review cypherpunkpay logs to:
    # * make sure cypherpunkpay did start
    # * understand what cypherpunkpay is doing
    sudo journalctl -xe --unit cypherpunkpay.service
    
    # Open in your browser 
    http://127.0.0.1:8080/
    

### Congratulations![](#congratulations)

What we achieved here:

*   CypherpunkPay is live on your laptop or virtual machine.
    
*   CypherpunkPay is only accessible locally on the same machine at [http://127.0.0.1:8080/](http://127.0.0.1:8080/)
    
*   CypherpunkPay runs in **donations mode** (the default).
    
*   CypherpunkPay runs on **Bitcoin testnet** (so you can play safely).
    
*   A **testnet wallet got created** and configured for you during package installation.
    

### Next steps[](#next-steps)

For **production** we need a few more things, discussed over the next chapters. Here is a quick overview:

*   We will enable `cypherpunkpay` with `systemctl`.
    
*   We will put `cypherpunkpay` behind a reverse proxy and setup TLS certificate. Don’t worry, Caddy makes this trivial.
    
*   We will configure your target Bitcoin wallet to receive funds.
    
*   If you are a **merchant**, you will want to set an API callbacks to notify your store about payments status. This is **not** necessary if you just want to receive donations.
