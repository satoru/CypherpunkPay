Config File Reference
=====================

`/etc/cypherpunkpay.conf` is the default config file path. You can override it with `--config-file` option.

For an up-to-date list of possible config options see the source for [default config]. in repository > **CypherpunkPay/cypherpunkpay/config/cypherpunkpay_defaults.conf**
