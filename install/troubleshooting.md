Troubleshooting
===============

### CypherpunkPay layout is completely broken (CSS files not loading?)[](#cypherpunkpay-layout-is-completely-broken-css-files-not-loading)

See “CypherpunkPay renders HTTP links when HTTPS are expected”.

### CypherpunkPay renders HTTP links when HTTPS are expected[](#cypherpunkpay-renders-http-links-when-https-are-expected)

1.  Make sure CypherpunkPay only listens on `localhost`. It cannot listen not public interfaces (not `*`). See `listen` option in `/etc/cypherpunkpay.conf`.
    
2.  Make sure your reverse proxy (like nginx or apache) correctly sets related headers.
    
    For nginx, this goes inside the `location` block:
    
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Host $host:$server_port;
        proxy_set_header X-Forwarded-Port $server_port;
        
    
    Apache:
    
        RequestHeader set X-Forwarded-Proto https
        
    
3.  Make sure you did NOT overrided these options in your `/etc/cypherpunkpay.conf`:
    
        [server]
        
        listen = 127.0.0.1:8080
        
        trusted_proxy = 127.0.0.1
        trusted_proxy_count = 1
        trusted_proxy_headers = x-forwarded-for x-forwarded-host x-forwarded-proto x-forwarded-port
        clear_untrusted_proxy_headers = yes
        
    

For details, see [waitress docs](https://docs.pylonsproject.org/projects/waitress/en/latest/reverse-proxy.html?highlight=url_scheme#using-behind-a-reverse-proxy).

#### CypherpunkPay won’t start - database lock not released[](#cypherpunkpay-wont-start---database-lock-not-released)

When you get the following errors on CypherpunkPay startup:

`yoyo.exceptions.LockTimeout: Process has locked this database (run yoyo break-lock to remove this lock)`

`sqlite3.IntegrityError: UNIQUE constraint failed: yoyo_lock.locked`

Run this (adjusting paths if needed):

`/opt/venvs/cypherpunkpay/bin/yoyo break-lock --database sqlite:////var/lib/cypherpunkpay/backup/cypherpunkpay.sqlite3`

### How to reinstall /etc/cypherpunkpay.conf after deletion?[](#how-to-reinstall-etccypherpunkpayconf-after-deletion)

The `apt` package manager does **not** recreate deleted config files when reinstalling the package. This is not CypherpunkPay specific.

To force recreating `/etc/cypherpunkpay.conf`:

    apt-get install --reinstall -o Dpkg::Options::="--force-confask,confnew,confmiss" cypherpunkpay
    

For details, see [discussion on Ubuntu Stack Exchange](https://web.archive.org/web/20211017130243/https://askubuntu.com/questions/66533/how-can-i-restore-configuration-files).
