Enable and start systemd service
================================

CypherpunkPay comes with a systemd `cypherpunkpay.service` which allows you to manage it with `systemctl` command.

    # Enable cypherpunkpay on system startup
    sudo systemctl enable cypherpunkpay
    
    # Start cypherpunkpay now
    sudo systemctl start cypherpunkpay
    
    # Verify cypherpunkpay is running
    sudo systemctl status cypherpunkpay
    
    # Check cypherpunkpay logs
    sudo journalctl -xe --unit cypherpunkpay.service
    
