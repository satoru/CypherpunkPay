CypherpunkPay vs BTCPay Server
==============================

### BTCPay Server is legendary[](#btcpay-server-is-legendary)

[BTCPay Server](https://btcpayserver.org/) is legendary software known for making [ShitPay](https://bitpay.com/) obsolete. It was our inspiration. BTCPay Server is great and fully featured self-hosted payment processor you should definitely take time to review.

BTCPay Server is designed to be a full merchant wallet if you provide it with a private key. In this advanced scenario, it supports Lightning Network, PayJoin and more.

### What is not ideal about BTCPay Server[](#what-is-not-ideal-about-btcpay-server)

The drawback, in our opinion, is **deployment and maintenance complexity and cost**.

BTCPay Server is composed of many moving parts to the point manual installation and wiring up is [not recommended by the authors](https://docs.btcpayserver.org/ManualDeployment/).

Instead, you can go with the preconfigured docker image, which does work but adds further complexity and opaqueness to DevOps and SecOps (including custom updates mechanism). There is also a very easy “one-click” deployment option but that is specific to a handful of credit-card-required public clouds.

In our view, many security critical operations cannot afford this level of complexity.

### CypherpunkPay advantages[](#cypherpunkpay-advantages)

We believe CypherpunkPay advantage is simplicity.

*   A single linux daemon
*   Linux packages and updates
*   Instant on, upgrade to full node when ready
*   $5 VPS compatible
*   Designed to work with onion services and Tor Browser as first class citizens; clearnet an afterthought
