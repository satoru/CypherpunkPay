Accessing Admin Panel
=====================

> Register immediately after enabling the admin panel. The registration page will be public.

### Enable admin panel[](#enable-admin-panel)

In `/etc/cypherpunkpay.conf` uncomment or add:

    admin_panel_enabled = true
    

### Restart CypherpunkPay[](#restart-cypherpunkpay)

    sudo systemctl restart cypherpunkpay
    

### Learn admin panel URL[](#learn-admin-panel-url)

A unique admin panel URL is generated for each CypherpunkPay instance. You can learn it from logs:

    sudo journalctl -u cypherpunkpay -g admin
    

Example output:

`Admin panel enabled at /cypherpunkpay/admin/eeec6kyl2rqhys72b6encxxrte/`

Append the path to your domain and visit the URL.

### Register your admin user[](#register-your-admin-user)

Registration page will only be available until you registered the first (and only) user.

Enjoy!
